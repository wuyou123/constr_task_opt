#ifndef CONSTR_TASK_OPT_H
#define CONSTR_TASK_OPT_H

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include <qpOASES.hpp>

#include <iostream>
#include <assert.h>  
#include <vector>

class CParams
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    CParams()
    {
        int num_vars = 0;           
        int num_constr = 0;         
        double tau_der = 0.0;
        double l1 = 0.0;
        double l2 = 0.0;
        double tstep = 0.0;            
        double tstep_horizon = 0.0;    
        int nWSR = 0;               
        double time_cpu_max = 0.0;  
    };
    ~CParams(){};

    int num_vars;          // Number of problem variables (e.q. DoFs).
    int num_constr;        // Number of problem contraints.
    double tau_der;       // Time constant for high-frequency pole in derivative computation.
    double l1;            // Gronwall parameter for position.  
    double l2;            // Gronwall parameter for velocity.  
    double tstep;            // Time step.
    double tstep_horizon;    // Prediction horizon time step.
    int nWSR;             // Number of working set recalculations (max. iter.).
    double time_cpu_max;  // Max. CPU duration (time-out).
};

class CTask
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    CTask(const int& task_dim, const int& num_vars, const bool& is_pos, const bool& norm_jac, const Eigen::VectorXd& task_weights)
    {
        dim = task_dim;
        dofs = num_vars;
        is_pos_space = is_pos;
        normalize_jac = norm_jac;
        weights = task_weights;
        error = Eigen::VectorXd::Zero(dim);
        Jac = Eigen::MatrixXd::Zero(dim, num_vars);
    };
    ~CTask(){};

    int dim;            // DOFs in task space (Jacobian rows)
    int dofs;           // DOFs in task space (Jacobian rows)
    bool is_pos_space;  // Set task in position (true) or velocity (false) space
    bool normalize_jac; // Normalize Jacobian when weighting with other tasks or not (false if 0's)
    Eigen::VectorXd error;     // error
    Eigen::MatrixXd Jac;       // Jacobian
    Eigen::VectorXd weights;   // Weights
};

class CTaskFull : public CTask
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    CTaskFull(const CTask& task): CTask(task.dim,task.dofs,task.is_pos_space,task.normalize_jac,task.weights)
    {
        fill(task);
        Jac_old = Jac;
        DJac = Eigen::MatrixXd::Zero(dim, Jac.cols());
        DJac_old = DJac;
        A = Eigen::MatrixXd::Zero(dim, Jac.cols());
        b = Eigen::VectorXd::Zero(dim);
        Weights = task.weights.asDiagonal();       
    };
    ~CTaskFull(){};

    void fill(const CTask& task)
    {
        error = task.error;
        Jac = task.Jac;
        weights = task.weights;       
        Weights = weights.asDiagonal();       
    };

    Eigen::MatrixXd Jac_old;   // Jacobian in previous optimization
    Eigen::MatrixXd DJac;      // Jacobian Derivative
    Eigen::MatrixXd DJac_old;  // Jacobian Derivative in previous optimization
    Eigen::MatrixXd A;         // System matrix
    Eigen::VectorXd b;         // System vector
    Eigen::MatrixXd Weights;   // Weights
};


class CConstr
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    CConstr(const int& constr_dim, const int& num_vars, const bool& is_pos, const int& btype)
    {
        dim = constr_dim;
        dofs = num_vars;
        bound_type = btype; 
        is_pos_space = is_pos;
        error = Eigen::VectorXd::Zero(constr_dim);
        Jac = Eigen::MatrixXd::Zero(constr_dim,num_vars);
    };
    ~CConstr(){};

    int dim;            // Constraint dimension (Jacobian rows)
    int dofs;           // Constraint dimension (Jacobian rows)
    bool is_pos_space;  // Set task in position (true) or velocity (false) space
    int bound_type;     // 0 if 0<f, 1 if f<0, 2 if 0=f=0, any other number deactivates the constraint
    Eigen::VectorXd error;     // error
    Eigen::MatrixXd Jac;       // Jacobian
};

class CConstrFull : public CConstr
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    CConstrFull(const CConstr& constr) : CConstr(constr.dim, constr.dofs, constr.is_pos_space, constr.bound_type)
    {
        fill(constr);
        Jac_old = Jac;
        DJac = Eigen::MatrixXd::Zero(dim, Jac.cols());
        DJac_old = DJac;
        A = Eigen::MatrixXd::Zero(dim, Jac.cols());
        lb = Eigen::VectorXd::Zero(dim);
        ub = Eigen::VectorXd::Zero(dim);
    };
    ~CConstrFull(){};

    void fill(const CConstr& constr)
    {
        error = constr.error;
        Jac = constr.Jac;
        bound_type = constr.bound_type;
    };

    Eigen::MatrixXd Jac_old;   // Jacobian in previous optimization
    Eigen::MatrixXd DJac;      // Jacobian Derivative
    Eigen::MatrixXd DJac_old;  // Jacobian Derivative in previous optimization
    Eigen::MatrixXd A;         // System matrix
    Eigen::VectorXd lb;        // Lower system vector
    Eigen::VectorXd ub;        // Upper system vector
};

class CJoints
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    CJoints(){};
    ~CJoints(){};

    Eigen::MatrixXd Weights_joints; // Weights for the Optimization. The bigger they are, the more the corresponding variables will move;
    Eigen::VectorXd curr_pos;       // Current position
    Eigen::VectorXd curr_vel;       // current velocity
    Eigen::VectorXd ref_pos;        // Output reference to control position
    Eigen::VectorXd ref_vel;        // Output reference to control velocity
    Eigen::VectorXd ref_acc;        // Output reference to control acceleration
    Eigen::VectorXd ref_acc_dual;   // Output reference to control acceleration (dual solution)
};

class CBounds
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    CBounds(){};
    ~CBounds(){};

    Eigen::VectorXd lpos;  // Lower Position bounds
    Eigen::VectorXd upos;  // Upper Position bounds
    Eigen::VectorXd lvel;  // Lower Velocity bounds
    Eigen::VectorXd uvel;  // Upper Velocity bounds
    Eigen::VectorXd lacc;  // Lower Acceleration bounds
    Eigen::VectorXd uacc;  // Upper Acceleration bounds
};

class CQpoases
{
private:

    void get_solution(void)
    {
        this->sq_problem_ptr_->getPrimalSolution(xOpt.data());
        this->sq_problem_ptr_->getDualSolution(yOpt.data());
    };

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    CQpoases(const int& num_vars, const int& num_constr, const int& max_iter)
    {
        H = Eigen::MatrixXd::Identity(num_vars,num_vars);
        g = Eigen::VectorXd::Zero(num_vars);
        lb = -qpOASES::INFTY * Eigen::VectorXd::Ones(num_vars);
        ub = qpOASES::INFTY * Eigen::VectorXd::Ones(num_vars);
        A = Eigen::MatrixXd::Zero(num_constr, num_vars);
        lbA = -qpOASES::INFTY * Eigen::VectorXd::Ones(num_constr);
        ubA = qpOASES::INFTY * Eigen::VectorXd::Ones(num_constr);
        nWSR = max_iter;

        xOpt = Eigen::VectorXd::Zero(num_vars);
        yOpt = Eigen::VectorXd::Zero(num_vars+num_constr);

        sq_problem_ptr_ = new qpOASES::SQProblem(num_vars, num_constr);

        init();
    };
    ~CQpoases()
    {
        if (sq_problem_ptr_ != NULL)
        {
            delete sq_problem_ptr_;
            sq_problem_ptr_ = NULL;
        }
    };
    Eigen::MatrixXd H;     // Hessian matrix (num_vars x num_vars)
    Eigen::VectorXd g;     // Gradient vector (num_vars)
    Eigen::VectorXd lb;    // Lower bound vector (num_vars)
    Eigen::VectorXd ub;    // Upper bound vector (num_vars)
    Eigen::MatrixXd A;     // Constraint Matrix (num_constr x num_vars)
    Eigen::VectorXd lbA;   // Constraint's lower bound vector (num_vars)
    Eigen::VectorXd ubA;   // Copnstraint's upper bound vector (num_vars)
    int nWSR;       // Number of working set recalculations (max. iter.)
    Eigen::VectorXd xOpt;  // Solution (DoF acc. reference)
    Eigen::VectorXd yOpt;  // Dual solution

    qpOASES::SQProblem* sq_problem_ptr_; // qpOASES QProblem class pointer

    void init(void)
    {
        qpOASES::Options options;
        options.printLevel = qpOASES::PL_LOW;
        this->sq_problem_ptr_->setOptions(options);
        this->sq_problem_ptr_->init(H.data(), g.data(), A.data(), lb.data(), ub.data(), lbA.data(), ubA.data(), nWSR, 0);

        get_solution();
        

        // DEBUG
        // options.print();
        //print_vars();
    };

    void print_vars(void)
    {
        std::cout << "H: " << std::endl << "[" << H << "]" << std::endl;
        std::cout << "g: " << std::endl << "[" << g.transpose() << "]" << std::endl;
        std::cout << "A: " << std::endl << "[" << A << "]" << std::endl;
        std::cout << "lbA: [" << lbA.transpose() << "]" << std::endl;
        std::cout << "ubA: [" << ubA.transpose() << "]" << std::endl;
        std::cout << "lb: [" << lb.transpose() << "]" << std::endl;
        std::cout << "ub: [" << ub.transpose() << "]" << std::endl;
        std::cout << "nxOpt: [" <<  xOpt.transpose();
        std::cout << "]   yOpt = [" << yOpt.transpose();
        std::cout << "]   objVal = " <<  this->sq_problem_ptr_->getObjVal() << std::endl;
    }

    void hotstart(int lnWSR)
    {
        this->sq_problem_ptr_->hotstart(H.data(), g.data(), A.data(), lb.data(), ub.data(), lbA.data(), ubA.data(), lnWSR, 0);
        get_solution();
    };
};

class CConstrTaskOpt
{
  private:

    CParams params_;
    std::vector<CTaskFull> tasks_;      // Tasks
    std::vector<CConstrFull> constrs_;  // Constraints
    CJoints joints_;                    // Robot joints
    CBounds bounds_;                    // Problem bounds (lower and upper bounds of acc. vel. and pos.)
    bool params_init_;                  // Flag to check if params have been initialized.
    bool tasks_init_;                   // Flag to check if tasks have been initialized.
    bool constrs_init_;                 // Flag to check if constraints have been initialized.
    CQpoases* qpoases_ptr_;             // QPoases class object

    /**
    * \brief Derivative Matrix
    *
    * Get derivative of the matrix.
    */
    void derivative_matrix(const double& tau_filt, const double& Tsample, const Eigen::MatrixXd& Jm, Eigen::MatrixXd& dJm_dt, Eigen::MatrixXd& Jm_old, Eigen::MatrixXd& dJm_old_dt);

    /**
    * \brief Compute cost functional
    *
    * Compute cost functional.
    */
    void compute_cost_function(void);

    /**
    * \brief Compute A and b
    *
    * Compute matrix A and vector b of the form Ax+b = 0;
    */
    void compute_Ab(const Eigen::VectorXd& f, const Eigen::MatrixXd& Jac, const Eigen::MatrixXd& DJac_dt, const Eigen::VectorXd& csi_dot, const double& l1, const double& l2, bool is_pos, bool normalize_jac, Eigen::MatrixXd& A, Eigen::VectorXd& b);

    /**
    * \brief Compute Hessian and gradient vector
    *
    * Compute Hessian and gradient vector.
    */
    void compute_Hg(const Eigen::MatrixXd& A, const Eigen::VectorXd& b, const Eigen::MatrixXd& W, Eigen::MatrixXd& H, Eigen::VectorXd& g);

    /**
    * \brief Compute Constraints
    *
    * Compute constraints matrix A and constraint bound vectors.
    */
    void compute_constraints(void);

    /**
    * \brief Compute Constraints A
    *
    * Compute lower and upper bounds of Constraints (lbA, ubA).
    */
    void compute_constraints_A(Eigen::VectorXd& error, Eigen::MatrixXd& Jac, Eigen::MatrixXd& DJac, const int& dim, const int& num_vars, int& boundtype_JC, Eigen::VectorXd& csi_dot, double& l1, double& l2, bool& is_pos, Eigen::VectorXd& lbA, Eigen::VectorXd& ubA, Eigen::MatrixXd& AJC);

    /**
    * \brief Compute Problem Bounds
    *
    * Compute lower and upper problem bounds (lb, ub).
    */
    void compute_prblm_bounds(void);

    /**
    * \brief Compute References
    *
    * Compute position, velocity and accelerations to command the joints (control references).
    */
    void compute_ctrl_ref(void);

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    /**
    * \brief Class Constructor
    *
    * Class constructor of the constrained task optimization class
    */
    CConstrTaskOpt(const int& num_vars, const int& num_constr, const int& nWSR);

    /**
    * \brief Class Destructor
    *
    * Class destructor of the constrained task optimization class
    */
    ~CConstrTaskOpt();

    /**
    * \brief Set parameters
    *
    * Sets all required parameters into main class objects.
    */
    void set_parameters(const CParams& params, const CBounds& bounds);

    /**
    * \brief Set bounds
    *
    * Sets problem bounds required by solver.
    */
    void set_bounds(const CBounds& bounds);

    /**
    * \brief Set tasks
    *
    * Sets all tasks informations into main class objects.
    */
    void set_tasks(const std::vector<CTask>& tasks);

    /**
    * \brief Set constraints
    *
    * Sets all constraints informations into main class objects.
    */
    void set_constraints(const std::vector<CConstr>& constrs);

    /**
    * \brief Set joints
    *
    * Sets current joint positions and velocities into main class objects.
    */
    void set_joints(const Eigen::VectorXd& jpos, const Eigen::VectorXd& jvel, const Eigen::VectorXd& weights_joints);

    /**
    * \brief Call solver
    *
    * Calls qpOASES solver to obtain MPC control references.
    */
    void call_solver(Eigen::VectorXd& csidd_solution, Eigen::VectorXd& csidd_dual_solution, Eigen::MatrixXd& csid_ref, Eigen::MatrixXd& csi_ref);
};


#endif
